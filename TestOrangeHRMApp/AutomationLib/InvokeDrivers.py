from selenium import webdriver

from AutomationLib.Utils import Utilities


def StartChromeDriver():
    # WebTest.Driver=webdriver.Chrome(executable_path="D:\\XoriantPythonSeleniumPostmanTraining\\Drivers\\chromedriver.exe")
    from  AutomationLib.WebTest import WebTest
    WebTest.Driver = webdriver.Chrome(
        executable_path=Utilities.Utils.EnvVars["ChromeDriverPath"])

def StartFirefoxDriver():
    from AutomationLib.WebTest import WebTest
    WebTest.Driver = webdriver.Firefox(
        executable_path=Utilities.Utils.EnvVars["FirefoxDriverPath"])
def StartEdgeDriver():
    pass