from AutomationLib.InvokeDrivers import StartChromeDriver, StartFirefoxDriver, StartEdgeDriver

BrowserDriverSwitcher = {
    "Chrome": StartChromeDriver,
    "Firefox":StartFirefoxDriver,
    "Edge":StartEdgeDriver

}

