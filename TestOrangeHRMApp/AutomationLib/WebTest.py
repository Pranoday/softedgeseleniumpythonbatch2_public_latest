from AutomationLib.BrowserSwitcher import BrowserDriverSwitcher
from AutomationLib.Utils.Utilities import Utils


class WebTest():

    Driver=None
    def __init__(self):
        self.ObjectRepo={}

    def StartTest(self,BrowserName):
        self.StartBrowser(BrowserName)
        self.OpenURL()

    def StartBrowser(self,BrowserName):
        func=BrowserDriverSwitcher.get(BrowserName)
        func()

    def OpenURL(self):
        WebTest.Driver.get(Utils.EnvVars["ApplicationURL"])

    def CreateObjectRepository(self,FileName):
        pass

    def EnterText(self,Element,TextToEnter):
        Element.clear()
        Element.send_keys(TextToEnter)

    def ClickElement(self,Element):
        Element.click()

    def ClearText(self,Element):
        Element.clear()

    def AppendText(self,Element,TextToAppend):
        Element.send_keys(TextToAppend)

