''''
    1.Open Browser
    2.Open URL "https://opensource-demo.orangehrmlive.com/"
    3.Enter "Admin" in UserName field
    4.Enter "admin123" in Password field
    5.Click on Login button
    6.Dashboard page is opened successfully

'''
from Pages.LoginPage import LoginPage

Lp1=LoginPage()
Lp1.DoLogin("Admin","admin123")
CurrentURL=Lp1.GetCurrentURL()
assert CurrentURL=="https://opensource-demo.orangehrmlive.com/index.php/dashboard","Wrong page opened"
