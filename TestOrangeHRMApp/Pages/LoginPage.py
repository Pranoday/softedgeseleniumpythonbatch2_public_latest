from AutomationLib.Utils import Utilities
from Pages.BasePage import BasePage


class LoginPage(BasePage):
    def __init__(self):
        Utilities.Utils.ReadEnvVars()
        self.T.StartTest("Chrome")
        self.T.CreateObjectRepository("LoginPage")

    def DoLogin(self,UserName,Password):
        #UserNameField=driver.findElmentById("txtUsername")
        #UserNameField.send_keys(UserName)
        self.T.EnterText(self.T.ObjectRepo["UserNameField"],UserName)
        self.T.EnterText(self.T.ObjectRepo["PasswordField"],Password)
        self.T.ClickElement(self.T.ObjectRepo["LoginButton"])
        #GenderDropdown = driver.findElmentById("txtUsername")
        #SelectValueFromDropdown(GenderDropdown,"BY_VISIBLETEXT","MALE")