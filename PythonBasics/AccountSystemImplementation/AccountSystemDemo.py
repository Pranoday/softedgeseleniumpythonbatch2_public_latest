from AccountSystemImplementation.Account import Account
from AccountSystemImplementation.InsufficientBalanaceException import InsuffiencentBalanceException
from AccountSystemImplementation.SavingsAccount import SavingsAccount

Act1=Account(1,100)
Act1.DepositeAmount(1000)
CurrentBalance=Act1.GetBalance()
print("Account NO: "+str(Act1.GetAccountNo())+" has balance: "+str(Act1.GetBalance()))


Act2=Account(2,1000)
Act2.DepositeAmount(10000)
try:
    Act2.WithdrawAmount(25000)
    CurrentBalance=Act2.GetBalance()
    print("Account NO: "+str(Act2.GetAccountNo())+" has balance: "+str(Act2.GetBalance()))
except:
    print("Amount is not withdrawn as you do not have sufficient balance")
Act3=Account(3,1000)
Act3.DepositeAmount(1000)
try:
    Act3.WithdrawAmount(5000)
    CurrentBalance=Act3.GetBalance()
    print("After withdrawal Account NO: "+str(Act3.GetAccountNo())+" has balance: "+str(Act3.GetBalance()))
except:
    print("Amount is not withdrawn as you do not have sufficient balance")

Act3.EarnInterest()
CurrentBalance = Act3.GetBalance()
print("After earning Account NO: "+str(Act3.GetAccountNo())+" has balance: "+str(Act3.GetBalance()))

Act3.DepositeAmount()
CurrentBalance = Act3.GetBalance()
print("After Withdrawal with No argument Account NO: "+str(Act3.GetAccountNo())+" has balance: "+str(Act3.GetBalance()))

Act3.DepositeAmount(4000)
CurrentBalance = Act3.GetBalance()
print("After Withdrawal with 1 argument Account NO: "+str(Act3.GetAccountNo())+" has balance: "+str(Act3.GetBalance()))


Act4=Account(4)

CurrentBalance = Act4.GetBalance()
print("Initial Balance of Account NO: "+str(Act4.GetAccountNo())+" has balance: "+str(Act4.GetBalance()))

SAct1=SavingsAccount(5,5000)
try:
    SAct1.WithdrawAmount(5000)
    CurrentBalance = SAct1.GetBalance()
    print("After withdrawal Balance of Account NO: "+str(SAct1.GetAccountNo())+" has balance: "+str(SAct1.GetBalance()))
except InsuffiencentBalanceException:
    print("Your account does not have sufficient balance")

SAct1.OpenAccount()