import pytest

from CalculatorAPI.CalculatorAPI import Calculator

Calc=None
Res=0
@pytest.fixture()
def CreateObject():
    # Even if we have a global variable with name Calc,here new local variable
    # with the name Calc will be created.So this local Calc and global Calc are different
    # Calc=Calculator()
    # We are mentioning to use Global variable Calc and assign it the object of
    # Calculator class
    # global Calc is ONLY clarifying to use already defined global variable
    # global keyword does not define global variables,when variable is defined outside
    # functions,it means it is indeed in global scope.
    # global keyword just asks to use variable from global scope instead of creating
    # new variable in local scope
    global Calc
    global Res
    Calc = Calculator()
    yield
    Res=0
    print("Reinitialising Res to 0 after the execution of each test")
def testAdditionWithPositiveValues(CreateObject):
    global Res
    Res=Calc.Addition(20,30)
    assert 50==Res,"Addition not working with Positive values"

def testAdditionWithPositiveAndNegativeValues(CreateObject):
    global Res
    Res = Calc.Addition(-20, 30)
    assert 10 == Res, "Addition not working with Positive and Negative values"

def testAdditionWithZeroNumbers(CreateObject):
    global Res
    Res = Calc.Addition(0, 0)
    assert Res == 0

def testAdditionWithZeroAndNonZeroNumbers(CreateObject):
    global Res
    Res = Calc.Addition(0, 100)
    assert Res == 100

