import pytest


@pytest.mark.usefixtures("CreateObjectForUeInTestClass")
class TestAdditionWithPositiveValues():
    Cal=None

    def testAdditionWithPositiveValues(self,ProvidePositiveValues):
        Res = TestAdditionWithPositiveValues.Cal.Addition(ProvidePositiveValues[0], ProvidePositiveValues[1])
        assert ProvidePositiveValues[2] == Res, "Addition not working with Positive values"

    def testAdditionWithPositiveValuesUsingDic(self, ProvidePositiveValuesUsingDictionary):
        Res = TestAdditionWithPositiveValues.Cal.Addition(ProvidePositiveValuesUsingDictionary["Num1"], ProvidePositiveValuesUsingDictionary["Num2"])
        assert ProvidePositiveValuesUsingDictionary["ExpectedResult"] == Res, "Addition not working with Positive values"
