import pytest

from CalculatorAPI.CalculatorAPI import Calculator


@pytest.fixture()
def CreateObject():
    #If we want to return anything to the testcase method from fixture
    #we can do that with the help of return

    print("This is a fixture from conftest")
    Calc = Calculator()
    return Calc

@pytest.fixture(scope="class")
def CreateObjectForUeInTestClass(request):
    request.cls.Cal = Calculator()

@pytest.fixture(params=[(10,20,30),(100,200,300),(10000,20000,30000)])
def ProvidePositiveValues(request):
    return request.param

@pytest.fixture(params=[{"Num1":40,"Num2":20,"ExpectedResult":60},{"Num1":400,"Num2":200,"ExpectedResult":600},{"Num1":10000,"Num2":30000,"ExpectedResult":40000}])
def ProvidePositiveValuesUsingDictionary(request):
    return request.param
