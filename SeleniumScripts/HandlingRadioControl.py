from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
Radio1=driver.find_element_by_id("benzradio")
Radio1.click()
time.sleep(5)
if(Radio1.is_selected()==True):
    print("Radio control got selected after clicking..PASSED")
else:
    print("Radio control did not get selected after clicking..FAILED")

Radio2=driver.find_element_by_id("hondaradio")
Radio2.click()
time.sleep(5)
if(Radio1.is_selected()==False and Radio2.is_selected()==True):
    print("Radio controls work fine..PASSED")
else:
    print("Radio controls not woring fine..FAILED")
driver.close()