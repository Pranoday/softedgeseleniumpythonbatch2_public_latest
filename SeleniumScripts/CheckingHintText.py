from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")

NameField=driver.find_element_by_name("enter-name")
HintText=NameField.get_attribute("placeholder")
if(HintText=="Enter Your Name"):
    print("Correct Hinttext is displayed..PASSED")
else:
    print("Correct Hinttext is NOT displayed..FAILED")
driver.close()