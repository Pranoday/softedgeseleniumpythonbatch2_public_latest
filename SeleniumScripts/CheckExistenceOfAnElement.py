from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
HideButton=driver.find_element_by_id("hide-textbox")
HideButton.click()
IsElementFound=0
try:
    driver.find_element_by_id("displayed-text")
except NoSuchElementException:
    IsElementFound=1
if(IsElementFound==1):
    print("Hide button has successfully removed the element..PASSED")
else:
    print("Hide button has successfully not removed the element..FAILED ")

ShowButton=driver.find_element_by_id("show-textbox")
ShowButton.click()
IsElementFound=0
try:
    driver.find_element_by_id("displayed-text")
except NoSuchElementException:
    IsElementFound=1

if(IsElementFound==0):
    print("Show button has successfully added the element back..PASSED")
else:
    print("Show button has not added element back..FAILED ")

