from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
HoverButton=driver.find_element_by_id("mousehover")
driver.execute_script("arguments[0].scrollIntoView({block:\"center\"});",HoverButton)
time.sleep(5)
Act=ActionChains(driver)
Act.move_to_element(HoverButton).perform()
time.sleep(5)
driver.find_element_by_link_text("Top").click()