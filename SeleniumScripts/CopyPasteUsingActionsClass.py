from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://opensource-demo.orangehrmlive.com/")
UserNameField=driver.find_element_by_id("txtUsername")
UserNameField.send_keys("Pranoday")

Act=ActionChains(driver)
Act.key_down(Keys.CONTROL).send_keys("a").key_up(Keys.CONTROL).perform()
Act.key_down(Keys.CONTROL).send_keys("c").key_up(Keys.CONTROL).perform()

PasswordField=driver.find_element_by_id("txtPassword")
Act.key_down(Keys.CONTROL,element=PasswordField).send_keys("v").key_up(Keys.CONTROL).perform()

PasswordVal=PasswordField.get_attribute("value")
if(PasswordVal==""):
    print("Password can not be pasted..PASSED")
else:
    print("Password can  be pasted..FAILED")

