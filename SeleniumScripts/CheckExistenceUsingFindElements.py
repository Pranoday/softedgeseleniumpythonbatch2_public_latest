from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
HideButton=driver.find_element_by_id("hide-textbox")
HideButton.click()

List1=driver.find_elements_by_id("displayed-text")
if(len(List1)==0):
    print("Hide button worked fine..PASSED")
else:
    print("Hide button did not work fine..FAILED")

ShowButton=driver.find_element_by_id("show-textbox")
ShowButton.click()

List1=driver.find_elements_by_id("displayed-text")
if(len(List1)==1):
    print("Show button worked fine..PASSED")
else:
    print("Show button did not work fine..FAILED")
