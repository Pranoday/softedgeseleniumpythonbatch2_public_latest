from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
Checkboxes=driver.find_elements_by_css_selector("input[type='checkbox']")
#Checkboxes=driver.find_elements_by_name("cars")
for Checkbox in Checkboxes:
    Checkbox.click()
    time.sleep(3)
    if(Checkbox.is_selected()==True):
        print("Checkbox is checked successfully")
    else:
        print("Checkbox is NOT checked successfully")
