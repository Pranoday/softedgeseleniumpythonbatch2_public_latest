from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_input_test")
driver.switch_to.frame("iframeResult")
FirstNameField=driver.find_element_by_id("fname")
FirstNameField.send_keys("Pranoday")
FirstName=FirstNameField.get_attribute("value")

LastNameField=driver.find_element_by_name("lname")
LastNameField.send_keys("Dingare")
LastName=LastNameField.get_attribute("value")

SubmitBtn=driver.find_element_by_css_selector("input[type='submit']")
SubmitBtn.click()
time.sleep(2)
DivElement=driver.find_element_by_css_selector("div[class='w3-container w3-large w3-border']")
DivText=DivElement.text
if(FirstName in DivText and LastName in DivText):
    print("Correct data is submitted..passed")
else:
    print("Correct data is NOT submitted..failed")
driver.switch_to.default_content()
driver.find_element_by_id("runbtn").click()