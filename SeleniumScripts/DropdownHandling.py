from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
DropdownElement=driver.find_element_by_id("carselect")
Se=Select(DropdownElement)
SelectedOptionElement=Se.first_selected_option
SelectedOptionText=SelectedOptionElement.text
if(SelectedOptionText=="BMW"):
    print("Correct value is selected by default..PASSED")
else:
    print("Correct value is NOT selected by default..FAILED")

Se.select_by_index(2)
time.sleep(2)
Se.select_by_visible_text("Benz")
time.sleep(2)
Se.select_by_value("bmw")
time.sleep(2)

SelectedOptionElement=Se.first_selected_option
SelectedOptionText=SelectedOptionElement.text
if(SelectedOptionText=="BMW"):
    print("Correct value is selected after clicking..PASSED")
else:
    print("Correct value is NOT selected after clicking..FAILED")

ExpectedCarNames=["BMW","Benz","Honda"]
AllOptions=Se.options
for Option in AllOptions:
    OptionText=Option.text
    if(OptionText in ExpectedCarNames):
        print("Correct option is present..PASSED")
    else:
        print("Correct option is NOT present..FAILED")