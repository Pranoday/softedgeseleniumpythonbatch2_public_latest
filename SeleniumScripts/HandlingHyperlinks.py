from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://www.w3schools.com/html/tryit.asp?filename=tryhtml_links_w3schools")
driver.switch_to.frame("iframeResult")
driver.find_element_by_link_text("Visit W3Schools.com!").click()
driver.back()
#driver.find_element_by_partial_link_text("Logout")
